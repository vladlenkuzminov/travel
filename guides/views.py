from .forms import GuideForm
from .models import Guid
from .models import City
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from registrations.forms import AccountForm
from registrations.models import Account
from tours.models import Tour
from utils.views import render_to


@render_to('guides.html')
def overview(request):
    if 'id_city' in request.session:
        guides = Guid.objects.filter(city=request.session['id_city'])
        return {'guides': guides}
    else:
        guides = Guid.objects.all()
        return {'guides': guides}


@render_to('add-guide.html')
def add_guide(request):
    id_cities = Tour.objects.values_list('city', flat=True).distinct()
    cities = City.objects.filter(id__in=id_cities)

    if request.method == 'POST':
        guide_form = GuideForm(request.POST)
        account_form = AccountForm(request.POST)
        if guide_form.is_valid() and account_form.is_valid():
            account = account_form.save()
            guide = guide_form.save(commit=False)
            guide.account = account
            guide.save()
            return HttpResponseRedirect(reverse('manage_guides:overview'))
    else:
        # import ipdb; ipdb.set_trace()
        guide_form = GuideForm()
        guide_form['cities'].field.queryset = cities
        account_form = AccountForm()

    return {'guideForm': guide_form, 'accountForm': account_form}


def delete_guide(request, id):
    guide = Guid.objects.get(pk=id)
    Account.objects.get(email=guide.account).delete()
    guide.delete()
    return HttpResponseRedirect(reverse('manage_guides:overview'))


@render_to('edit-guide.html')
def edit_guide(request, id):
    guide = get_object_or_404(Guid, pk=id)
    id_cities = Tour.objects.values_list('city', flat=True).distinct()
    cities = City.objects.filter(id__in=id_cities)

    if request.method == 'POST':
        guide_form = GuideForm(request.POST, instance=guide)
        # guide_form['cities'].field.queryset = cities
        account_form = AccountForm(request.POST, instance=guide.account)
        if guide_form.is_valid() and account_form.is_valid():
            guide_form.save()
            account_form.save()
            return HttpResponseRedirect(reverse('manage_guides:overview'))
    else:
        # import ipdb; ipdb.set_trace()
        guide_form = GuideForm(instance=guide)
        guide_form['cities'].field.queryset = cities
        account_form = AccountForm(instance=guide.account)

    return {'guideForm': guide_form, 'accountForm': account_form}
