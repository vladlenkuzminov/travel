from django.contrib import admin
from .models import Guid, Country, City, Language, PaymentMethod

# Register your models here.
admin.site.register(Country)
admin.site.register(City)
admin.site.register(Language)
admin.site.register(PaymentMethod)
admin.site.register(Guid)
