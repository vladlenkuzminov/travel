# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-27 07:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('City', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Country', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Countries',
            },
        ),
        migrations.CreateModel(
            name='GuidInformation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstName', models.CharField(max_length=20)),
                ('lastName', models.CharField(max_length=25)),
                ('phoneNumber', models.CharField(max_length=13)),
                ('email', models.EmailField(max_length=254)),
                ('address', models.CharField(max_length=30)),
                ('bankAccount', models.CharField(max_length=30)),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='guides.City')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='guides.Country')),
            ],
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('language', models.CharField(choices=[('EN', 'English'), ('RU', 'Russian')], default='EN', max_length=2)),
            ],
        ),
        migrations.CreateModel(
            name='PaymentMethod',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('paymentmethod', models.CharField(choices=[(1, 'Visa'), (2, 'MasterCard'), (3, 'Cash')], max_length=1)),
            ],
        ),
        migrations.AddField(
            model_name='guidinformation',
            name='language',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='guides.Language'),
        ),
        migrations.AddField(
            model_name='guidinformation',
            name='paymentMethod',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='guides.PaymentMethod'),
        ),
    ]
