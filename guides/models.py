from django.db import models
from registrations.models import Account
# Create your models here.


class Country(models.Model):
    country = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = "Countries"

    def __str__(self):
        return self.country


class City(models.Model):
    city = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = "Cities"

    def __str__(self):
        return self.city


class Language(models.Model):
    LANG = (
        ('EN', "English"),
        ('RU', "Russian")
    )
    language = models.CharField(max_length=2, choices=LANG, default="EN")

    def __str__(self):
        return self.language


class PaymentMethod(models.Model):
    PM = (
        ('Visa', 'Visa'),
        ('MasterCard', 'MasterCard'),
        ('Cash', 'Cash')
    )
    paymentmethod = models.CharField(max_length=10, choices=PM)

    def __str__(self):
        return self.paymentmethod


class Guid(models.Model):
    address = models.CharField(max_length=30, blank=True)
    bankAccount = models.CharField(max_length=30, blank=True)
    country = models.ForeignKey(Country)
    city = models.ForeignKey(City, related_name='citytoguid')
    language = models.ForeignKey(Language)
    paymentMethod = models.ForeignKey(PaymentMethod)
    account = models.OneToOneField(Account, blank=True)
    cities = models.ManyToManyField(City, related_name='citiestoguid', blank=True)

    def __str__(self):
        return self.address
