from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.overview, name='overview'),
    url(r'^addguide/$', views.add_guide, name='add_guide'),
    url(r'^editguide/([0-9]+)$', views.edit_guide, name='edit_guide'),
    url(r'^deleteguide/([0-9]+)$', views.delete_guide, name='delete_guide'),

]
