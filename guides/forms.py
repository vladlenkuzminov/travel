from django import forms
from .models import Guid


class GuideForm(forms.ModelForm):
    
    class Meta:
        model = Guid
        fields = [
            'address', 'bankAccount', 'country', 'city', 'language', 'paymentMethod', 'cities'
        ]
        widgets = {
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'bankAccount': forms.TextInput(attrs={'class': 'form-control'}),
            'country': forms.Select(attrs={'class': 'form-control'}),
            'city': forms.Select(attrs={'class': 'form-control'}),
            'language': forms.Select(attrs={'class': 'form-control'}),
            'paymentMethod': forms.Select(attrs={'class': 'form-control'}),
            'cities': forms.CheckboxSelectMultiple()

        }
        labels = {
            'address': 'Address',
            'bankAccount': 'Bank account',
            'country': 'Country',
            'city': 'City',
            'lang': 'Language',
            'paymentmethod': 'Payment method',
            'cities': 'Cities'
        }
