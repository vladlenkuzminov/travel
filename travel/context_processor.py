from guides.models import City


def getcities(request):
    # import ipdb; ipdb.set_trace()
    allCities = City.objects.all()
    if 'id_city' in request.session:
        return {'Cities': allCities, 'City': City.objects.get(pk=request.session['id_city'])}

    else:
        return {'Cities': allCities, 'City': 'All'}


def getusername(request):
    # import ipdb; ipdb.set_trace()
    return {'User': request.user.email}
