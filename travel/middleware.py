class CitiesMiddleware(object):

    def process_request(self, request):
        # import ipdb; ipdb.set_trace()
        if 'id_city' in request.GET:
            request.session['id_city'] = request.GET.get('id_city')

        if 'id_city' in request.session and request.GET.get('id_city') == 'all':
            request.session.pop('id_city')
