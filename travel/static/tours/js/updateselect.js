$(document).ready(function() {
  $('#id_country').change(function() {
    var optionSelected = $("option:selected", this);
    var country = optionSelected.text();
    $('#id_city option').each(function() {
      $(this).show();
      s = $(this).text();
      if ((s.indexOf(country) + 1) === 0){
          $(this).hide();
     }
    });
  });
});
