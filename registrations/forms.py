from django import forms
from .models import Account


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = [
            'first_name', 'last_name', 'email', 'phoneNumber', 'password'
        ]
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'phoneNumber': forms.TextInput(attrs={'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control'})
        }
        labels = {
            'first_name': 'First name',
            'last_name': 'Last name',
            'email': 'Email',
            'phoneNumber': 'Phone number',
            'password': 'Password'
        }

    def save(self, commit=True):
        user = super(AccountForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user
