from .forms import AccountForm
from guides.forms import GuideForm
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from utils.views import render_to
from stronghold.decorators import public


@public
@render_to('page-register.html')
def sign_up(request):
    if request.method == 'POST':
        # import ipdb; ipdb.set_trace()
        account_form = AccountForm(request.POST)
        guide_form = GuideForm(request.POST)
        if account_form.is_valid():
            account = account_form.save()
            if guide_form.is_valid():
                guide_form.account = account
                guide = guide_form.save(commit=False)
                guide.account = account
                guide.save()
            return HttpResponseRedirect(reverse('registrations:sign_up'))
    else:
        account_form = AccountForm()
        guide_form = GuideForm()

    return {'AccountForm': account_form, 'GuideForm': guide_form}
