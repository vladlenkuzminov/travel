from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views


urlpatterns = [
    url(r'^$', views.sign_up, name='sign_up'),
    url(r'^login/$', auth_views.login, {'template_name': 'page-login.html'},
        name='login'),  # name login
    url(r'^logout/$', auth_views.logout, name='logout'),
]
