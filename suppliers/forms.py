from django import forms
from .models import Supplier, ContactPerson


class SupplierForm(forms.ModelForm):

    class Meta:
        model = Supplier
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'country': forms.Select(attrs={'class': 'form-control'}),
            'city': forms.Select(attrs={'class': 'form-control'}),
            'kind': forms.Select(attrs={'class': 'form-control'}),
            'postCode': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'tour': forms.CheckboxSelectMultiple(),
            'phoneNumber': forms.TextInput(attrs={'class': 'form-control'}),
            'minGroup': forms.NumberInput(attrs={'class': 'form-control'}),
            'maxGroup': forms.NumberInput(attrs={'class': 'form-control'})
        }
        labels = {
            'name': 'Company',
            'address': 'Address',
            'country': 'Country',
            'city': 'City',
            'kind': 'Type',
            'postCode': 'Post code',
            'tour': 'Tour',
            'email': 'Email',
            'phoneNumber': 'Phone number',
            'minGroup': 'Min group',
            'maxGroup': 'Max group'
        }


class ContactPersonForm(forms.ModelForm):

    class Meta:
        model = ContactPerson
        fields = '__all__'
        widgets = {
            'firstName': forms.TextInput(attrs={'class': 'form-control'}),
            'lastName': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'phoneNumber': forms.TextInput(attrs={'class': 'form-control'}),
            'supplier': forms.Select(attrs={'class': 'form-control'})
        }
        labels = {
            'firstName': 'First name',
            'lastName': 'Last name',
            'email': 'Email',
            'phoneNumber': 'Phone number',
            'supplier': 'Supplier'
        }
