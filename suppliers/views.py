from .forms import ContactPersonForm
from .forms import SupplierForm
from .models import ContactPerson
from .models import Supplier
from django.core.urlresolvers import reverse
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from tours.models import Tour
from utils.views import render_json
from utils.views import render_to


@render_to('suppliers.html')
def overview(request):
    if 'id_city' in request.session:
        supplers = Supplier.objects.filter(city=request.session['id_city'])
        return {'Suppliers': supplers}
    else:
        supplers = Supplier.objects.all()
        return {'Suppliers': supplers}


@render_to('add-supplier.html')
def add_supplier(request):

    contactFormSet = inlineformset_factory(
        Supplier, ContactPerson, extra=1, form=ContactPersonForm)
    if request.method == 'POST':
        # import ipdb; ipdb.set_trace()
        supplier_form = SupplierForm(request.POST)
        contact_formset = contactFormSet(request.POST)
        if supplier_form.is_valid() and contact_formset.is_valid():

            instance = supplier_form.save()
            contact_formset.instance = instance
            contact_formset.save()

            return HttpResponseRedirect(reverse('manage_suppliers:overview'))

    else:
        supplier_form = SupplierForm()
        contact_formset = contactFormSet(queryset=ContactPerson.objects.none())
    return {
        'supplierform': supplier_form,
        'contactformset': contact_formset
    }


@render_json
def get_tours_add(request):
    if request.is_ajax() and request.method == 'GET':
        tours = Tour.objects.filter(city=request.GET.get('city_id'))
        if tours:
            tours_data = [(tour.id, tour.title) for tour in tours]
            return {
                'success': True,
                'data': {'tours': tours_data}
            }

    return {'success': False}


@render_json
def get_tours_edit(request):
    if request.is_ajax() and request.method == 'GET':
        tours = Tour.objects.filter(city=request.GET.get('city_id'))
        supplier = Supplier.objects.get(pk=request.GET.get('supplier_id'))

        if tours:
            tours_data = [(tour.id, tour.title, True) if tour in supplier.tour.all() else (tour.id, tour.title, False) for tour in tours]
            return {
                'success': True,
                'data': {'tours': tours_data}
            }

    return {'success': False}


@render_to('edit-supplier.html')
def edit_supplier(request, id):
    supplier = get_object_or_404(Supplier, pk=id)
    supplier_form = SupplierForm(instance=supplier)
    tours = Tour.objects.filter(city=supplier.city)
    supplier_form.fields['tour'].queryset = tours
    contactFormSet = inlineformset_factory(
        Supplier, ContactPerson, extra=0, form=ContactPersonForm)
    contact_formset = contactFormSet(instance=supplier)
    if request.method == 'POST':

        supplier_form = SupplierForm(request.POST, instance=supplier)
        if supplier_form.is_valid():
            supplier_instance = supplier_form.save(commit=False)
            contact_formset = contactFormSet(request.POST, instance=supplier_instance)
            if contact_formset.is_valid():
                supplier_form.save()
                contact_formset.save()
                return HttpResponseRedirect(reverse('manage_suppliers:overview'))
    return {
        'supplierform': supplier_form,
        'contactformset': contact_formset,
        'supplier_id': id
    }


def delete_supplier(request, id):
    instance = get_object_or_404(Supplier, pk=id)
    ContactPerson.objects.filter(supplier=instance).delete()
    Supplier.objects.filter(pk=id).delete()
    return HttpResponseRedirect(reverse('manage_suppliers:overview'))
