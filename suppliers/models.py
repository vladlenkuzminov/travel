from django.db import models
from guides.models import City
from guides.models import Country
from tours.models import Tour


class Kind(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Supplier(models.Model):
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=30)
    country = models.ForeignKey(Country)
    city = models.ForeignKey(City)
    kind = models.ForeignKey(Kind)
    postCode = models.IntegerField()
    tour = models.ManyToManyField(Tour)
    email = models.EmailField()
    phoneNumber = models.CharField(max_length=13)
    minGroup = models.SmallIntegerField()
    maxGroup = models.SmallIntegerField()

    def __str__(self):
        return self.name


class ContactPerson(models.Model):
    firstName = models.CharField(max_length=20)
    lastName = models.CharField(max_length=25)
    email = models.EmailField()
    phoneNumber = models.CharField(max_length=13)
    supplier = models.ForeignKey(Supplier)

    def __str__(self):
        return self.firstName + " " + self.lastName
