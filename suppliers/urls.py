from . import views
from django.conf.urls import url


urlpatterns = [
    url(r'^$', views.overview, name='overview'),
    url(r'^addsupplier/$', views.add_supplier, name='add_supplier'),
    url(r'^editsupplier/([0-9]+)$', views.edit_supplier, name='edit_supplier'),
    url(r'^gettoursedit/$', views.get_tours_edit, name='get_tours_edit'),
    url(r'^gettoursadd/$', views.get_tours_add, name='get_tour_sadd'),
    url(r'^delete/([0-9]+)$', views.delete_supplier, name='delete_supplier')
]