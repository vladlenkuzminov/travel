from django.contrib import admin
from .models import Kind, Supplier, ContactPerson


admin.site.register(ContactPerson)
admin.site.register(Supplier)
admin.site.register(Kind)
