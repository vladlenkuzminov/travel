from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.overview, name='overview'),
    url(r'^addtour/$', views.add_tour, name='add_tour'),
    url(r'^showstops/([0-9]+)$', views.show_stops, name='show_stops'),
    url(r'^edittour/([0-9]+)$', views.edit_tour, name='edit_tour'),
    url(r'^deletetour/([0-9]+)$', views.delete_tour, name='delete_tour'),
    url(r'^deletestop/([0-9]+)$', views.delete_stop, name='delete_stop'),
]
