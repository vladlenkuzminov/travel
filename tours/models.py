from django.db import models
from guides.models import City

# Create your models here.


class Kind(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Tour(models.Model):
    title = models.CharField(max_length=50)
    city = models.ForeignKey(City, related_name="tourtocities")
    kind = models.ForeignKey(Kind, related_name="tourtokinds")
    description = models.CharField(max_length=100, blank=True)
    duration = models.DurationField()
    minGroup = models.SmallIntegerField()
    maxGroup = models.SmallIntegerField()
    pricePerson = models.SmallIntegerField()
    priceChild = models.SmallIntegerField()

    def __str__(self):
        return self.title


class Suplier(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Stop(models.Model):
    name = models.CharField(max_length=30)
    startTime = models.DateTimeField()
    duration = models.DurationField()
    description = models.CharField(max_length=100, blank=True)
    tour = models.ForeignKey(Tour)
    suplier = models.ManyToManyField(Suplier)

    def __str__(self):
        return self.name
