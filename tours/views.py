from .forms import StopForm
from .forms import TourForm
from .models import Stop
from .models import Tour
from django.core.urlresolvers import reverse
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from utils.views import render_to
# Create your views here.


@render_to('tours.html')
def overview(request):
    if 'id_city' in request.session:
        tours = Tour.objects.filter(city=request.session['id_city'])
    else:
        tours = Tour.objects.all()

    if request.method == 'POST':
        query = request.POST.get("search")

        if query:
            tours = tours.filter(title__contains=query)

    return {'Tours': tours}


@render_to('show-stops.html')
def show_stops(request, id):
    tour = get_object_or_404(Tour, pk=id)
    stopFormSet = inlineformset_factory(Tour, Stop, extra=0, form=StopForm)
    stop_formset = stopFormSet(instance=tour)
    return {'stopformset': stop_formset}


@render_to('add-tour.html')
def add_tour(request):
    stopFormSet = inlineformset_factory(Tour, Stop, extra=1, form=StopForm)

    if request.method == 'POST':
        tour_form = TourForm(request.POST)
        stop_formset = stopFormSet(request.POST)
        if tour_form.is_valid() and stop_formset.is_valid():
            instance = tour_form.save()
            stop_formset.instance = instance
            stop_formset.save()
            return HttpResponseRedirect(reverse('manage_tours:overview'))
    else:
        stop_formset = stopFormSet(queryset=Stop.objects.none())
        tour_form = TourForm()
        # import ipdb; ipdb.set_trace()
    return {'tourform': tour_form, 'stopformset': stop_formset}


def delete_tour(request, id):
    instance = get_object_or_404(Tour, pk=id)
    Stop.objects.filter(tour=instance).delete()
    Tour.objects.get(pk=id).delete()
    return HttpResponseRedirect(reverse('manage_tours:overview'))


def delete_stop(request, id):
    Stop.objects.get(pk=id).delete()
    return HttpResponseRedirect(reverse('manage_tours:overview'))


@render_to('edit-tour.html')
def edit_tour(request, id):
    tour = get_object_or_404(Tour, pk=id)
    tour_form = TourForm(instance=tour)
    stopFormSet = inlineformset_factory(Tour, Stop, extra=0, form=StopForm)
    stop_formset = stopFormSet(instance=tour)

    if request.method == 'POST':
        tour_form = TourForm(request.POST, instance=tour)
        tour = tour_form.save(commit=False)
        stop_formset = stopFormSet(request.POST, instance=tour)
        if tour_form.is_valid() and stop_formset.is_valid():
            tour_form.save()
            stop_formset.save()
            return HttpResponseRedirect(reverse('manage_tours:overview'))

    return {'tourform': tour_form, 'stopformset': stop_formset}
