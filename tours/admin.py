from django.contrib import admin
from .models import Kind, Tour, Stop, Suplier


admin.site.register(Tour)
admin.site.register(Kind)
admin.site.register(Stop)
admin.site.register(Suplier)