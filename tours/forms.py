from django import forms
from .models import Tour, Stop


class TourForm(forms.ModelForm):
    class Meta:
        model = Tour
        fields = [
            'title', 'city', 'kind', 'description', 'duration',
            'minGroup', 'maxGroup', 'pricePerson', 'priceChild'
        ]
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'city': forms.Select(attrs={'class': 'form-control'}),
            'kind': forms.Select(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 3}),
            'duration': forms.TextInput(attrs={'class': 'form-control'}),
            'minGroup': forms.NumberInput(attrs={'class': 'form-control'}),
            'maxGroup': forms.NumberInput(attrs={'class': 'form-control'}),
            'pricePerson': forms.NumberInput(attrs={'class': 'form-control'}),
            'priceChild': forms.NumberInput(attrs={'class': 'form-control'})
        }
        labels = {
            'title': 'Title',
            'city': 'City',
            'kind': 'Type',
            'description': 'Description',
            'duration': 'Duration',
            'minGroup': 'Min group',
            'maxGroup': 'Max group',
            'pricePerson': 'Per person',
            'priceChild': 'Per child'
        }


class StopForm(forms.ModelForm):
    class Meta:
        model = Stop
        fields = ['name', 'startTime', 'duration', 'description', 'suplier']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'startTime': forms.TextInput(attrs={'class': 'form-control'}),
            'duration': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 3}),
            'suplier': forms.SelectMultiple(attrs={'id':'custom-headers', 'multiple': 'multiple'})
        }
